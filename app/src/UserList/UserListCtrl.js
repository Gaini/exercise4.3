'use strict'

userApp.controller('UserListCtrl', function ($q, $scope, UsersService, PostsService) {
    $scope.loadingUsers = true;
    $scope.loadingPosts = true;

    var requestUsers = UsersService.getUsers().then(function (response) {
        $scope.users = response.data
    }).finally(function () {
    });

    var requestPosts = PostsService.getPosts().then(function (response) {
        $scope.posts = response.data
    }).finally(function () {
    });

    $q.all([requestUsers, requestPosts]).then(function(values){
        $scope.loadingUsers = false;
        $scope.loadingPosts = false;
    });


    /*   UsersService.getUsers().then(function (response) {
        $scope.users = response.data
        return PostsService.getPosts()
      }).then(function (response) {
        $scope.posts = response.data
      }) */

})
