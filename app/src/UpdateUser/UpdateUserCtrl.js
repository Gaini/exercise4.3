'use strict'

userApp.controller('UpdateUserCtrl', function ($scope, UsersService, $routeParams) {

    $scope.userId = $routeParams.userId;

    UsersService.getUser($scope.userId).then(function (response) {
        $scope.currentUser = response.data
    });

    $scope.updateUser = function (myUser) {

        UsersService.updateUser(myUser, $scope.userId).then(function (response) {
            $scope.userId = response.data.id;
            $scope.currentUser = response.data;
        })
    }
})
